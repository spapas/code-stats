defmodule CodeStats.Mixfile do
  use Mix.Project

  def project do
    [
      app: :code_stats,
      version: "2.4.5",
      elixir: "~> 1.15",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix] ++ Mix.compilers(),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),

      # Docs
      name: "Code::Stats",
      source_url: "https://gitlab.com/code-stats/code-stats",
      homepage_url: "https://codestats.net",
      docs: [
        # The main page in the docs
        main: "readme",
        logo: "assets/logos/Logo-crushed.png",
        extras: ["README.md"]
      ],

      # Release configuration
      releases: [
        code_stats: [
          steps: [:assemble, :tar]
        ]
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {CodeStats, []},
      extra_applications: [:logger]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(:ci), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.6.6"},
      {:postgrex, ">= 0.15.3"},
      {:phoenix_pubsub, "~> 2.0"},
      {:phoenix_ecto, "~> 4.4"},
      {:ecto_sql, "~> 3.7"},
      {:phoenix_html, "~> 3.0"},
      {:phoenix_live_reload, "~> 1.3", only: :dev},
      {:jason, "~> 1.4"},
      {:gettext, "~> 0.23.1"},
      {:cowboy, "~> 2.7"},
      {:plug_cowboy, "~> 2.2"},
      {:comeonin, "~> 5.3"},
      {:bcrypt_elixir, "~> 3.0"},
      {:number, "~> 1.0"},
      {:earmark, "~> 1.4", only: :dev},
      {:ex_doc, "~> 0.30.9", only: :dev},
      {:bamboo, "~> 2.2"},
      {:bamboo_phoenix, "~> 1.0"},
      {:corsica, "~> 2.1"},
      {:appsignal_phoenix, "~> 2.1"},
      {:mbu, "~> 3.0.0", runtime: false},
      {:geolix, "~> 2.0"},
      {:geolix_adapter_mmdb2, "~> 0.6.0"},
      {:remote_ip, "~> 1.1"},
      {:absinthe, "~> 1.5"},
      {:absinthe_plug, "~> 1.5"},
      {:ex2ms, "~> 1.6"},
      {:csv, "~> 3.2"},
      {:tzdata, "~> 1.0"},
      {:finch, "~> 0.16.0"},
      {:dotenv_parser, "~> 2.0", only: :dev},
      {:phoenix_live_view, "~> 0.18.3"},
      {:language_colours, "~> 1.1"},
      {:rainbow, "~> 0.1.0"},
      {:floki, ">= 0.30.0", only: [:test, :ci]}
    ]
  end

  # Aliases are shortcut or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"],
      rel: ["compile", "frontend.build", "phx.digest", "release"]
    ]
  end
end
