defmodule CodeStats do
  use Application

  alias CodeStatsWeb.Gravatar

  import CodeStats.Utils, only: [get_conf: 1]

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    lc_conf = Application.fetch_env!(:language_colours, :databases).colour_db

    children = [
      # Start the Ecto repository first for database access
      {CodeStats.Repo, []},
      {Finch, name: CodeStats.HTTPClient},

      # PubSub runs channel system
      {Phoenix.PubSub, [name: CodeStats.PubSub]},

      # Other PubSubs
      {Registry,
       keys: :duplicate, name: CodeStats.User.Pulse.PubSub, partitions: System.schedulers_online()},
      {Registry,
       keys: :duplicate, name: CodeStats.User.Cache.PubSub, partitions: System.schedulers_online()},

      # Get historical XP data to cache
      {CodeStats.XPHistoryCache, [name: CodeStats.XPHistoryCache]},

      # Start Gravatar proxy
      {Gravatar.Proxy, %Gravatar.Proxy.Options{name: Gravatar.Proxy}},

      # Start language colour server
      {LanguageColours.ETSDatabase, LanguageColours.ETSDatabase.startup_options(lc_conf)},

      # Start the endpoint when the application starts
      {CodeStatsWeb.Endpoint, [name: CodeStatsWeb.Endpoint]},

      # Start the Terminator
      {CodeStats.User.Terminator, [name: CodeStats.User.Terminator]},

      # Manager of user licenses
      {CodeStats.User.Paid.Manager,
       %CodeStats.User.Paid.Manager.Options{
         name: CodeStats.User.Paid.Manager,
         check_interval: 2
       }}
    ]

    # Start XPCacheRefresher if in prod or if told to
    children =
      case {get_conf(:compile_env), get_conf(:run_caches)} do
        {:dev, nil} -> children
        _ -> children ++ [{CodeStats.XP.XPCacheRefresher, [name: CodeStats.XP.XPCacheRefresher]}]
      end

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: CodeStats.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    CodeStatsWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
