defmodule Mix.Tasks.Frontend.Build.Css do
  use MBU.BuildTask, auto_path: false, create_out_path: false
  @shortdoc "Build the frontend CSS"

  @deps [
    "frontend.build.css.copy"
  ]

  task _ do
    # Nothing to do
  end
end
