defmodule Mix.Tasks.Frontend.Watch do
  use MBU.BuildTask, auto_path: false, create_out_path: false
  import MBU.TaskUtils
  alias Mix.Tasks.Frontend.Build.Css.Compile, as: FrontendCompileCSS
  alias Mix.Tasks.Frontend.Build.Css.Copy, as: FrontendCopyCSS
  alias Mix.Tasks.Frontend.Build.Assets, as: FrontendAssets
  alias Mix.Tasks.Frontend.Build.Vendor, as: FrontendVendor
  alias Mix.Tasks.Frontend.Build.Js, as: FrontendJs
  alias Mix.Tasks.Frontend.Build.Misc, as: FrontendMisc

  @shortdoc "Watch frontend assets and rebuild when necessary"

  @deps [
    "frontend.build"
  ]

  task _ do
    watches = [
      watch(
        "CompileFrontendCSS",
        FrontendCompileCSS.watch_path(),
        FrontendCompileCSS
      ),
      watch(
        "CopyFrontendCSS",
        FrontendCopyCSS.in_path(),
        FrontendCopyCSS
      ),
      watch(
        "CopyFrontendAssets",
        FrontendAssets.in_path(),
        FrontendAssets
      ),
      watch(
        "CopyFrontendVendor",
        FrontendVendor.in_path(),
        FrontendVendor
      ),
      watch(
        "CopyFrontendJs",
        FrontendJs.in_path(),
        FrontendJs
      ),
      watch(
        "CopyFrontendMisc",
        FrontendMisc.in_path(),
        FrontendMisc
      )
    ]

    watches |> listen(watch: true)
  end
end
