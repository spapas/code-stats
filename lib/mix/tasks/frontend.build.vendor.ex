defmodule Mix.Tasks.Frontend.Build.Vendor do
  use MBU.BuildTask, auto_path: false
  import CodeStats.FrontendConfs

  @shortdoc "Copy frontend vendor assets to target dir"

  @deps []

  def in_path(), do: Path.join([base_src_path(), "node_modules"])
  def out_path(), do: Path.join([base_dist_path(), "vendor"])

  task _ do
    File.cp_r!(in_path(), out_path(), dereference_symlinks: true)
  end
end
