defmodule Mix.Tasks.Frontend.Build.Js do
  use MBU.BuildTask, auto_path: false
  import CodeStats.FrontendConfs

  @shortdoc "Copy JS assets to target dir"

  @deps []

  def in_path(), do: Path.join([base_src_path(), "js"])
  def out_path(), do: Path.join([base_dist_path(), "js"])

  task _ do
    File.cp_r!(in_path(), out_path(), dereference_symlinks: true)
  end
end
