defmodule CodeStats.DateUtils do
  @moduledoc """
  This module contains utilities related to date and time handling.
  """

  @doc ~S"""
  Convert the given UTC datetime and an offset into a naive local datetime, reconstructing the
  user's local time.

  ## Examples

      iex> {:ok, dt, _} = DateTime.from_iso8601("2019-01-15T15:00:00Z");
      ...> CodeStats.DateUtils.utc_and_offset_to_local(dt, 120)
      ~N[2019-01-15T17:00:00]
  """
  @spec utc_and_offset_to_local(DateTime.t(), integer()) :: NaiveDateTime.t()
  def utc_and_offset_to_local(%DateTime{} = date, offset) when is_integer(offset) do
    DateTime.add(date, offset, :minute) |> DateTime.to_naive()
  end

  @doc """
  Convert the given datetime into a RFC 3339 compliant timestamp string.

  ## Examples

      iex> CodeStats.DateUtils.to_rfc3339(~U[2019-01-15T15:00:00Z])
      "2019-01-15T15:00:00+00:00"

      iex> dt = DateTime.shift_zone!(~U[2019-01-15T15:00:00Z], "America/New_York");
      ...> CodeStats.DateUtils.to_rfc3339(dt)
      "2019-01-15T10:00:00-05:00"
  """
  @spec to_rfc3339(DateTime.t()) :: String.t()
  def to_rfc3339(%DateTime{} = dt) do
    base = Calendar.strftime(dt, "%Y-%m-%dT%H:%M:%S")

    <<sign::binary-size(1), hours::binary-size(2), minutes::binary-size(2)>> =
      Calendar.strftime(dt, "%z")

    <<base::binary, sign::binary, hours::binary, ":", minutes::binary>>
  end

  @doc """
  Get the date (in the year 2000) for a day of year integer (1..366). The day of year must have
  been generated using a leap year range for this to work properly.

  ## Examples

      iex> CodeStats.DateUtils.date_from_day_of_leap_year(1)
      ~D[2000-01-01]

      iex> CodeStats.DateUtils.date_from_day_of_leap_year(366)
      ~D[2000-12-31]

      iex> CodeStats.DateUtils.date_from_day_of_leap_year(60)
      ~D[2000-02-29]
  """
  def date_from_day_of_leap_year(doy) when doy in 1..366 do
    Date.new!(2000, 1, 1) |> Date.add(doy - 1)
  end
end
