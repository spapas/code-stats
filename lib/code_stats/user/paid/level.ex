defmodule CodeStats.User.Paid.Level do
  @moduledoc """
  User's paid levels. These are used for gating features for certain classes of paid users.
  """

  @level_none :user_level_none
  @level_adfree :user_level_adfree
  @level_adfree_all :user_level_adfree_all

  @typedoc "User's paid level"
  @type t() :: :user_level_none | :user_level_adfree | :user_level_adfree_all

  @doc """
  The "base" user level where user has no paid features.
  """
  @spec none :: :user_level_none
  def none(), do: @level_none

  @doc """
  Ad free level, user doesn't see ads.
  """
  @spec adfree :: :user_level_adfree
  def adfree(), do: @level_adfree

  @doc """
  Ad free for all level, user doesn't see ads and no one sees ads on their profile.
  """
  @spec adfree_all :: :user_level_adfree_all
  def adfree_all(), do: @level_adfree_all
end
