defmodule CodeStats.User.Paid.UserUtils do
  alias CodeStats.Repo

  @doc """
  Get a given user's paid account data.
  """
  @spec paid_user_for_user(CodeStats.User.t() | nil) :: CodeStats.PaidUser.t() | nil
  def paid_user_for_user(user)

  def paid_user_for_user(nil), do: nil

  def paid_user_for_user(user) do
    Repo.get!(CodeStats.PaidUser, user.id)
  end
end
