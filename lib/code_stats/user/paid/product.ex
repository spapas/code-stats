defmodule CodeStats.User.Paid.Product do
  import CodeStats.Utils.TypedSchema

  alias CodeStats.User.Paid.Level

  deftypedschema "products" do
    field(:product_id, :string, String.t())

    # Name of the variant in the Gumroad API
    field(:variant_name, :string, String.t())

    field(:unlock_level, Ecto.Enum, Level.t(),
      values: [
        Level.none(),
        Level.adfree(),
        Level.adfree_all()
      ]
    )
  end
end
