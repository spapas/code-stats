defmodule CodeStats.User.Paid.LicenseUtils do
  alias Gumroad.Api.Utils
  alias Gumroad.Api.PurchaseData

  @doc """
  How often (in days) to check for license's subscription's validity.
  """
  @spec license_check_interval() :: non_neg_integer()
  def license_check_interval(), do: CodeStats.Utils.get_conf!(:license_check_interval)

  @doc """
  Verify that the given license key is valid and the subscription attached to it isn't ended.
  """
  @spec verify_license(String.t()) ::
          {:ok, PurchaseData.t()} | :subscription_invalid | :not_found | :invalid | :error
  def verify_license(license) do
    product_permalink = Application.fetch_env!(:code_stats, :gumroad_product_permalink)

    with {:ok, resp} <- Utils.verify_license(product_permalink, license),
         true <- resp.success,
         # We don't care how many times a token has been used as long as it's only used for one
         # user at a time
         # 1 <- resp.uses,
         false <- resp.purchase.refunded,
         false <- resp.purchase.disputed do
      if resp.purchase.subscription_ended_at != nil or
           resp.purchase.subscription_failed_at != nil or
           (resp.purchase.subscription_cancelled_at != nil and
              not ended_license_still_valid?(resp.purchase)) do
        :subscription_invalid
      else
        {:ok, resp.purchase}
      end
    else
      :error -> :error
      :not_found -> :not_found
      _ -> :invalid
    end
  end

  @doc """
  Is this license, if the subscription has been ended, still valid?

  The subscription should be valid until the `subscription_cancelled_at` date, unless it has
  been refunded or disputed.
  """
  @spec ended_license_still_valid?(PurchaseData.t()) :: boolean()
  def ended_license_still_valid?(purchase_data)

  def ended_license_still_valid?(%PurchaseData{
        refunded: true
      }),
      do: false

  def ended_license_still_valid?(%PurchaseData{
        disputed: true
      }),
      do: false

  def ended_license_still_valid?(purchase_data) do
    now = Date.utc_today()

    Date.compare(now, purchase_data.subscription_cancelled_at) in [:lt, :eq]
  end
end
