defmodule CodeStats.PaidUser do
  @doc """
  Schema for a user's paid account information.
  """

  require Logger

  import CodeStats.Utils.TypedSchema
  import Ecto.Changeset

  alias CodeStats.User.Paid

  @paid_levels [
    Paid.Level.none(),
    Paid.Level.adfree(),
    Paid.Level.adfree_all()
  ]

  deftypedschema "users" do
    field(:paid_level, Ecto.Enum, Paid.Level.t(), values: @paid_levels)
    field(:license_key, :string, String.t() | nil)
    field(:subscription_id, :string, String.t() | nil)
    field(:subscription_valid_until, :date, Date.t())

    belongs_to(:paid_product, Paid.Product, Paid.Product.t() | nil)
  end

  @doc """
  Changeset that removes paid features from a user.
  """
  @spec invalidate_changeset(t()) :: Ecto.Changeset.t()
  def invalidate_changeset(user) do
    user
    |> change(%{
      paid_level: Paid.Level.none(),
      license_key: nil,
      subscription_id: nil,
      subscription_valid_until: nil,
      paid_product_id: nil
    })
  end

  @doc """
  Changeset for updating user's paid level.
  """
  @spec level_changeset(t(), map()) :: Ecto.Changeset.t()
  def level_changeset(user, params) do
    user
    |> cast(params, [:paid_level])
    |> validate_required([:paid_level])
    |> validate_inclusion(:paid_level, @paid_levels)
  end

  @doc """
  Changeset for changing the "valid until" timestamp of a user's subscription.
  """
  @spec subscription_validity_changeset(t(), map()) :: Ecto.Changeset.t()
  def subscription_validity_changeset(user, params) do
    user
    |> cast(params, [:subscription_valid_until])
    |> validate_required([:subscription_valid_until])
  end

  @doc """
  Changeset for updating user's license.
  """
  @spec license_changeset(t(), map()) :: Ecto.Changeset.t()
  def license_changeset(user, params) do
    changeset =
      user
      |> cast(params, [:license_key])
      |> validate_required([:license_key])
      |> unique_constraint(:license_key)

    key = get_change(changeset, :license_key)

    case key do
      nil ->
        changeset

      "" ->
        changeset
        |> put_change(:subscription_id, nil)
        |> put_change(:subscription_valid_until, nil)
        |> put_change(:paid_product_id, nil)
        |> put_change(:paid_level, Paid.Level.none())
        |> put_change(:license_key, nil)

      _ ->
        with {:ok, purchase_data} <- Paid.LicenseUtils.verify_license(key),
             {:prod, %Paid.Product{} = product} <-
               {:prod, Paid.ProductUtils.get_product_for_variant(purchase_data.variants)} do
          changeset
          |> put_change(:subscription_id, purchase_data.subscription_id)
          |> put_change(:subscription_valid_until, Date.utc_today() |> Date.add(30))
          |> put_change(:paid_product_id, product.id)
          |> put_change(:paid_level, product.unlock_level)
        else
          {:prod, _} ->
            add_error(changeset, :license_key, "has unknown product variant for license key")

          error ->
            add_error(changeset, :license_key, Atom.to_string(error))
        end
    end
  end
end
