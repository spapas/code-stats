defmodule CodeStats.Profile.Queries.Flow do
  import Ecto.Query, only: [from: 2]

  alias CodeStats.User.{Cache, CacheUtils, Flow}
  alias CodeStats.{Language, Repo}

  @top_languages_tpl %{
    id: nil,
    total_xp: 0,
    total_duration: 0,
    average_xp: 0.0,
    average_duration: 0.0,
    amount: 0,
    flow_score: 0
  }

  @type flow_languages() :: %{pos_integer() => integer()}

  @type meta_flow() :: %{
          start_time_local: NaiveDateTime.t(),
          xp: integer(),
          duration: pos_integer(),
          languages: flow_languages()
        }

  @type meta_info() :: %{
          longest: meta_flow() | nil,
          most_prolific: meta_flow() | nil,
          strongest: meta_flow() | nil,
          average_xp: float(),
          average_duration: float()
        }

  @type language_data() :: %{
          id: pos_integer(),
          total_xp: integer(),
          total_duration: pos_integer(),
          average_xp: float(),
          average_duration: float(),
          amount: pos_integer(),
          flow_score: pos_integer()
        }

  @type languages() :: %{optional(String.t()) => language_data()}

  @type int_languages() :: %{optional(pos_integer()) => language_data()}

  @type mins_per_day() :: %{date: Date.t(), mins: pos_integer()}

  @doc """
  Get amount of minutes spent in flow state per day since the given time.

  Note: days are calculated using local time.
  """
  @spec flow_mins_per_day(Cache.db_t(), Date.t()) :: {:ok, [mins_per_day()]}
  def flow_mins_per_day(cache, since) do
    data =
      cache
      |> Map.get("flows", [])
      |> Enum.reduce(%{}, fn flow, acc ->
        with {:ok, formatted} <- CodeStats.User.CacheUtils.unformat_flow(flow),
             date <- NaiveDateTime.to_date(formatted.start_time_local),
             v when v in [:eq, :gt] <- Date.compare(date, since) do
          Map.update(acc, date, formatted.duration, &(&1 + formatted.duration))
        else
          _ -> acc
        end
      end)
      |> Enum.map(fn {date, mins} -> %{date: date, mins: mins} end)

    {:ok, data}
  end

  @doc """
  Get the top languages programmed in a flow state.
  """
  @spec top_languages(Cache.t() | Cache.db_t()) :: {:ok, languages()}
  def top_languages(cache) do
    {data, lang_ids} =
      case cache do
        %Cache{} ->
          cache.flows

        db_cache ->
          Map.get(db_cache, "flows", [])
          |> Enum.map(fn flow ->
            case CacheUtils.unformat_flow(flow) do
              {:ok, f} -> f
              any -> any
            end
          end)
      end
      |> filter_flows()
      |> process_flows_languages()

    data =
      for {lang_id, d} <- data, into: %{} do
        lang_total =
          if is_struct(cache, Cache) do
            Map.fetch!(cache.languages, lang_id)
          else
            Map.fetch!(cache["languages"], to_string(lang_id))
          end

        {lang_id,
         %{
           d
           | average_xp: d.total_xp / d.amount,
             average_duration: round(d.total_duration / d.amount),
             total_duration: round(d.total_duration),
             flow_score: round(d.total_xp / lang_total * 100)
         }}
      end

    languages =
      from(l in Language, where: l.id in ^MapSet.to_list(lang_ids), select: {l.id, l.name})
      |> Repo.all()

    data =
      Enum.reduce(languages, data, fn {lang_id, lang_name}, acc ->
        lang_data = Map.fetch!(acc, lang_id)

        acc
        |> Map.put(lang_name, lang_data)
        |> Map.delete(lang_id)
      end)

    {:ok, data}
  end

  @doc """
  Get meta information about user's flows, like longest/strongest/etc.
  """
  @spec meta(Cache.db_t() | Cache.t()) :: {:ok, meta_info()}
  def meta(cache) do
    meta =
      case cache do
        %Cache{} ->
          cache.flows

        db_cache ->
          Map.get(db_cache, "flows", [])
          |> Enum.map(fn flow ->
            case CacheUtils.unformat_flow(flow) do
              {:ok, f} -> f
              any -> any
            end
          end)
      end
      |> filter_flows()
      |> Enum.reduce(
        %{
          longest: nil,
          most_prolific: nil,
          strongest: nil,
          total_xp: 0,
          total_duration: 0,
          total: 0
        },
        fn flow, acc ->
          longest =
            if is_nil(acc.longest) or
                 acc.longest.duration < flow.duration do
              flow
            else
              acc.longest
            end

          most_prolific =
            if is_nil(acc.most_prolific) or
                 acc.most_prolific.xp < flow.xp do
              flow
            else
              acc.most_prolific
            end

          strength = flow_strength(flow)

          strongest =
            if is_nil(acc.strongest) or flow_strength(acc.strongest) < strength do
              flow
            else
              acc.strongest
            end

          %{
            acc
            | longest: longest,
              most_prolific: most_prolific,
              strongest: strongest,
              total_xp: acc.total_xp + flow.xp,
              total_duration: acc.total_duration + flow.duration,
              total: acc.total + 1
          }
        end
      )

    average_xp = if meta.total > 0, do: meta.total_xp / meta.total, else: 0
    average_duration = if meta.total > 0, do: meta.total_duration / meta.total, else: 0

    {:ok,
     %{
       longest: format_meta_flow(meta.longest),
       most_prolific: format_meta_flow(meta.most_prolific),
       strongest: format_meta_flow(meta.strongest),
       average_xp: average_xp,
       average_duration: average_duration
     }}
  end

  @spec format_meta_flow(Flow.t()) :: meta_flow()
  def format_meta_flow(flow)

  def format_meta_flow(nil), do: nil

  def format_meta_flow(flow) do
    %{
      start_time_local: flow.start_time_local,
      xp: flow.xp,
      duration: flow.duration,
      languages: flow.languages
    }
  end

  @doc """
  Calculate strength of given flow, i.e. xp divided by duration.
  """
  @spec flow_strength(Flow.t() | meta_flow()) :: float()
  def flow_strength(flow) do
    try do
      flow.xp / flow.duration
    rescue
      ArithmeticError -> 0.0
    end
  end

  @spec process_flows_languages([Flow.t()]) :: {int_languages(), MapSet.t(pos_integer())}
  defp process_flows_languages(flows) do
    min_scaled = flow_min_scaled_duration()

    Enum.reduce(flows, {%{}, MapSet.new()}, fn flow, {data, lang_ids} ->
      flow_total_xp = Map.values(flow.languages) |> Enum.sum()

      data =
        process_flow_languages(flow.languages, flow.duration, flow_total_xp, min_scaled, data)

      lang_ids = data |> Map.keys() |> MapSet.new() |> MapSet.union(lang_ids)

      {data, lang_ids}
    end)
  end

  @spec process_flow_languages(
          %{optional(pos_integer()) => integer()},
          pos_integer(),
          integer(),
          float(),
          int_languages()
        ) :: int_languages()
  defp process_flow_languages(languages, duration, total_xp, min_scaled, total_data) do
    Enum.reduce(languages, total_data, fn {lang_id, lang_xp}, d ->
      # Scale duration so that it's based on the percentage of XP gained during the flow of
      # the total XP in the flow, to avoid gaining many minutes for very low XP. If scaled
      # duration is too low, ignore this whole flow.
      scaled_duration = duration * (lang_xp / total_xp)

      if scaled_duration > min_scaled do
        existing = Map.get(d, lang_id, @top_languages_tpl)

        Map.put(d, lang_id, %{
          existing
          | total_xp: existing.total_xp + lang_xp,
            total_duration: existing.total_duration + scaled_duration,
            amount: existing.amount + 1,
            id: lang_id
        })
      else
        d
      end
    end)
  end

  @spec filter_flows([f]) :: [f] when f: Flow.t() | Flow.cache_t()
  defp filter_flows(flows) do
    Enum.filter(flows, fn
      :invalid ->
        false

      %Flow{} = flow ->
        cond do
          flow.duration == 0 -> false
          flow.xp == 0 -> false
          flow.xp / flow.duration < Flow.min_xp_ratio() -> false
          true -> true
        end

      flow ->
        duration = Map.get(flow, Flow.cache_key(:duration), 0)
        amount = Map.get(flow, Flow.cache_key(:amount), 0)

        cond do
          duration == 0 -> false
          amount == 0 -> false
          amount / duration < Flow.min_xp_ratio() -> false
          true -> true
        end
    end)
  end

  # Minimum length of allowed flow, after scaling the flow based on language XP. The easing factor
  # is there to prevent a flow being discarded if it's just above the limit but it's split evenly
  # so that each language would have scaled duration below the minimum flow limit.
  @spec flow_min_scaled_duration() :: float()
  defp flow_min_scaled_duration(), do: Flow.min_length() * 0.3
end
