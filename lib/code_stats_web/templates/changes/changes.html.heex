<div class="stripe">
  <h1>Source</h1>

  <p>
    Code::Stats and its plugins are open source software, licensed under the 3-clause BSD licence. You can study the inner workings of the code and suggest improvements. You are also free to fork the code.
  </p>

  <p>
    <a target="_blank" href="https://gitlab.com/code-stats">
      View Code::Stats source code on GitLab →
    </a>
  </p>
</div>

<div class="stripe stripe-accent contributors">
  <h2>Contributors</h2>

  <div class="contributors-list">
    <div>
      <%= profile_pic(@conn, "nicd.png", "Mikko Ahlroth") %>

      <h3>Mikko Ahlroth</h3>
      <p class="subtitle">Lead developer</p>

      <p>
        <a target="_blank" href="https://blog.nytsoi.net/">Blog</a>
        / <a target="_blank" href="https://github.com/Nicd">GitHub</a>
        / <a target="_blank" href="https://gitlab.com/Nicd">GitLab</a>
        / <a target="_blank" href="https://social.ahlcode.fi/@nicd">Fediverse</a>
      </p>
    </div>

    <div>
      <img
        src={Routes.static_path(@conn, "/assets/frontend/images/Logo.svg")}
        alt="Code::Stats logo"
        class="avatar"
      />

      <h3>Hannu Hartikainen</h3>
      <p class="subtitle">
        <a target="_blank" href="https://gitlab.com/code-stats/code-stats-zsh">code-stats-zsh</a>
        &
        <a target="_blank" href="https://gitlab.com/code-stats/code-stats-vim">code-stats-vim</a>
        developer
      </p>

      <p>
        <a target="_blank" href="https://github.com/dancek">GitHub</a>
      </p>
    </div>

    <div>
      <%= profile_pic(@conn, "nikolauska.png", "Niko Lehtovirta") %>

      <h3>Niko Lehtovirta</h3>

      <p class="subtitle">Site developer</p>

      <p>
        <a target="_blank" href="https://github.com/nikolauska">GitHub</a>
        / <a target="_blank" href="https://gitlab.com/nikolauska">GitLab</a>
      </p>
    </div>

    <div>
      <%= profile_pic(@conn, "aman.jpeg", "Aman Mittal") %>

      <h3>Aman Mittal</h3>
      <p class="subtitle">
        <a target="_blank" href="https://gitlab.com/code-stats/codestats-cli">codestats-cli</a>
        developer
      </p>

      <p>
        <a target="_blank" href="https://github.com/amandeepmittal">GitHub</a>
      </p>
    </div>

    <div>
      <%= profile_pic(@conn, "riussi.jpg", "Juha Ristolainen") %>

      <h3>Juha Ristolainen</h3>
      <p class="subtitle">
        <a
          target="_blank"
          href="https://marketplace.visualstudio.com/items?itemName=riussi.code-stats-vscode"
        >
          code-stats-vscode
        </a>
        developer
      </p>

      <p>
        <a target="_blank" href="https://github.com/riussi">GitHub</a>
        / <a target="_blank" href="https://twitter.com/riussi">Twitter</a>
        / <a target="_blank" href="http://www.juharistolainen.com/">Site</a>
      </p>
    </div>

    <div>
      <%= profile_pic(@conn, "mastorm.png", "Mathias Storm") %>

      <h3>Mathias Storm</h3>
      <p class="subtitle">
        <a
          target="_blank"
          href="https://marketplace.visualstudio.com/items?itemName=MathiasSTorm.CodeStatsClient"
        >
          CodeStats.Client
        </a>
        developer
      </p>

      <p>
        <a target="_blank" href="https://github.com/mastorm">GitHub</a>
      </p>
    </div>

    <div>
      <%= profile_pic(@conn, "joebew42.jpeg", "JoeBew42") %>

      <h3>JoeBew42</h3>
      <p class="subtitle">Site developer</p>

      <p>
        <a target="_blank" href="https://github.com/joebew42">GitHub</a>
      </p>
    </div>

    <div>
      <%= profile_pic(@conn, "thehabbos007.jpg", "Ahmad Sattar") %>

      <h3>Ahmad Sattar</h3>
      <p class="subtitle">Site developer</p>

      <p>
        <a target="_blank" href="https://github.com/thehabbos007">GitHub</a>
        / <a target="_blank" href="https://gitlab.com/thehabbos007">GitLab</a>
      </p>
    </div>

    <div>
      <%= profile_pic(@conn, "jonathan.jpg", "Jonathan Böcker") %>

      <h3>Jonathan Böcker</h3>
      <p class="subtitle">
        <a target="_blank" href="https://github.com/Schwusch/codestats_flutter">
          Code::Stats Viewer
        </a>
        developer
      </p>

      <p>
        <a target="_blank" href="https://github.com/Schwusch/">GitHub</a>
      </p>
    </div>

    <div>
      <%= profile_pic(@conn, "luke.jpg", "Luke Gerhardt") %>

      <h3>Luke Gerhardt</h3>
      <p class="subtitle">
        <a target="_blank" href="https://bitbucket.org/Koohiisan/code-stats-vb6-add-in">
          Code::Stats VB6 Add-in
        </a>
        developer
      </p>

      <p>
        <a target="_blank" href="https://bitbucket.org/Koohiisan/">Bitbucket</a>
        / <a target="_blank" href="https://www.lukegerhardt.com/">Site</a>
      </p>
    </div>

    <div>
      <%= profile_pic(@conn, "bit01_social_logo_c.png", "Finn Dohrn (bitnulleins)") %>

      <h3>Finn Dohrn (bitnulleins)</h3>
      <p class="subtitle">
        <a target="_blank" href="https://gitlab.com/code-stats/code-stats-browser">
          code-stats-browser
        </a>
        developer
      </p>

      <p>
        <a target="_blank" href="https://blog.bit01.de/">Blog</a>
        / <a target="_blank" href="https://github.com/bitnulleins">GitHub</a>
      </p>
    </div>

    <div>
      <%= profile_pic(@conn, "avior.png", "Avior") %>

      <h3>Avior</h3>
      <p class="subtitle">
        <a target="_blank" href="https://github.com/Aviortheking/codestats-readme">
          codestats-readme
        </a>
        &amp;
        <a
          target="_blank"
          href="https://marketplace.visualstudio.com/items?itemName=riussi.code-stats-vscode"
        >
          code-stats-vscode
        </a>
        developer
      </p>

      <p>
        <a target="_blank" href="https://codestats.net/users/Aviortheking">Code::Stats profile</a>
        / <a target="_blank" href="https://github.com/Aviortheking">GitHub</a>
      </p>
    </div>

    <div>
      <%= profile_pic(@conn, "hxv.png", "hxv") %>

      <h3>hxv</h3>
      <p class="subtitle">
        Site developer
      </p>

      <p>
        <a target="_blank" href="https://codestats.net/users/hxv">Code::Stats profile</a>
        / <a target="_blank" href="https://fosstodon.org/@hxv">Mastodon</a>
      </p>
    </div>
  </div>
</div>

<div class="stripe">
  <h2>Changelog</h2>

  <%= for {version, date, title, items} <- @change_data do %>
    <CodeStatsWeb.Components.Changes.render_change
      version={version}
      date={date}
      title={title}
      items={items}
    />
  <% end %>
</div>
