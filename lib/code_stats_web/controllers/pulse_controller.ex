defmodule CodeStatsWeb.PulseController do
  use CodeStatsWeb, :controller
  require Logger

  alias Ecto.Changeset

  alias CodeStatsWeb.AuthUtils
  alias CodeStatsWeb.ProfileChannel
  alias CodeStatsWeb.FrontpageChannel
  alias CodeStatsWeb.GeoIPPlug
  alias CodeStatsWeb.Utils.CsvFormatter

  alias CodeStats.{Repo, Language, User, XP, Utils}
  alias CodeStats.User.{Machine, Pulse, CacheUtils}

  @datetime_max_diff 604_800
  @rfc3339_offset_regex ~R/(\+|-)(\d{2}):?(\d{2})$/
  @csv_pulse_timeout 300_000

  plug(GeoIPPlug when action in [:add])

  @spec list(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def list(conn, params) do
    since =
      with since_param when since_param != nil <- Map.get(params, "since"),
           {:ok, dt} <- Date.from_iso8601(since_param) do
        dt
      else
        _ -> nil
      end

    csv =
      AuthUtils.get_current_user_id(conn)
      |> XP.xps_by_user_id(since)
      |> Repo.all(timeout: @csv_pulse_timeout)
      |> CsvFormatter.format([
        "sent_at",
        "sent_at_local",
        "tz_offset",
        "language",
        "machine",
        "amount"
      ])

    conn
    |> put_resp_content_type("text/csv")
    |> put_resp_header("content-disposition", "attachment; filename=\"pulses.csv\"")
    |> send_resp(200, csv)
  end

  @doc """
  Action for adding a new pulse into the system.
  """
  @spec add(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def add(conn, params)

  def add(conn, %{"coded_at" => timestamp, "xps" => xps}) when is_list(xps) do
    {user, machine} = AuthUtils.get_machine_auth_details(conn)

    with {:ok, %DateTime{} = datetime} <- parse_timestamp(timestamp),
         {:ok, datetime} <- clamp_to_diff(datetime),
         {:ok, offset} <- get_offset(timestamp),
         {:ok, results} <- create_models(user, machine, datetime, offset, xps) do
      %{pulse: pulse, xps: created_xps, new_cache: new_cache} = Map.get(results, :pulse_add)

      # Structure xps for channel APIs
      formatted_xps =
        for %XP{language: l, amount: a} <- created_xps do
          %{language: l.name, amount: a}
        end

      # Coordinates are not sent for private profiles
      coords = if user.private_profile, do: nil, else: GeoIPPlug.get_coords(conn)

      # Broadcast XP data to possible viewers on profile page and frontpage
      ProfileChannel.send_pulse(user, pulse, machine, formatted_xps)

      CodeStats.User.Pulse.PubSub.publish(
        user,
        %Pulse{pulse | xps: created_xps, machine: machine},
        coords,
        new_cache
      )

      FrontpageChannel.send_pulse(coords, formatted_xps)

      conn |> put_status(201) |> json(%{ok: "Great success!"})
    else
      {:error, :generic, reason} ->
        conn |> put_status(400) |> json(%{error: reason})

      {:error, failed_operation, failed_value, _} ->
        Logger.error(
          "Failure in pulse add operation: #{inspect(failed_operation)} - #{inspect(failed_value)}"
        )

        conn
        |> put_status(500)
        |> json(%{error: "Failure in operation: #{inspect(failed_operation)}"})

      {:error, err} ->
        Logger.error("Unknown pulse add error: #{inspect(err)}")

        conn
        |> put_status(500)
        |> json(%{error: "Unknown error"})
    end
  end

  # Clause for invalid params
  def add(conn, _params) do
    conn
    |> put_status(400)
    |> json(%{error: "Invalid xps format."})
  end

  # Create the database models for the pulse in a transaction, also updating the user's cache
  @spec create_models(User.t(), Machine.t(), DateTime.t(), integer, [map]) ::
          {:ok, map} | {:error, any}
  defp create_models(user, machine, datetime, offset, input_xps) do
    try do
      Ecto.Multi.new()
      |> Ecto.Multi.run(:pulse_add, fn repo, _ ->
        with {:ok, pulse} <- create_pulse(repo, user, machine, datetime, offset),
             xps <- create_xps!(repo, user, pulse, input_xps),
             {:ok, new_cache} <- update_cache(repo, user, machine, datetime, offset, xps) do
          {:ok, %{pulse: pulse, xps: xps, new_cache: new_cache}}
        end
      end)
      |> Repo.transaction()
    rescue
      crash in RuntimeError -> {:error, :generic, crash.message}
      err -> {:error, {err, __STACKTRACE__}}
    end
  end

  # Update cache with new XP information, return updated
  @spec update_cache(Ecto.Repo.t(), User.t(), Machine.t(), DateTime.t(), integer, [XP.t()]) ::
          {:ok, User.Cache.t()}
  defp update_cache(
         repo,
         %User{} = user,
         %Machine{} = machine,
         %DateTime{} = datetime,
         offset,
         xps
       )
       when is_integer(offset) and is_list(xps) do
    local_datetime = CodeStats.DateUtils.utc_and_offset_to_local(datetime, offset)

    {cache, cache_db} =
      CacheUtils.update(
        user.cache,
        &CacheUtils.update_cache_from_xps(&1, datetime, local_datetime, machine.id, xps),
        false
      )

    cset = Changeset.cast(user, %{cache: cache_db}, [:cache])
    {:ok, _} = repo.update(cset)
    {:ok, cache}
  end

  @spec parse_timestamp(String.t()) :: {:ok, DateTime.t()} | {:error, atom, String.t()}
  defp parse_timestamp(timestamp) do
    err_ret = {:error, :generic, "Invalid coded_at format."}

    case DateTime.from_iso8601(timestamp) do
      {:ok, datetime, _} -> {:ok, datetime}
      {:error, _} -> err_ret
    end
  end

  @spec clamp_to_diff(DateTime.t()) :: DateTime.t()
  defp clamp_to_diff(datetime) do
    now = DateTime.utc_now()
    diff = DateTime.diff(now, datetime)

    cond do
      diff <= 0 -> {:ok, now}
      diff <= @datetime_max_diff -> {:ok, datetime}
      true -> {:error, :generic, "Invalid date."}
    end
  end

  @spec create_pulse(Ecto.Repo.t(), User.t(), Machine.t(), DateTime.t(), integer) ::
          {:ok, Pulse.t()} | {:error, Changeset.t()}
  defp create_pulse(repo, user, machine, datetime, offset) do
    # Create shifted naive datetime from UTC datetime and offset, recreating the user's
    # local time
    local_datetime = CodeStats.DateUtils.utc_and_offset_to_local(datetime, offset)

    params = %{
      "sent_at" => datetime,
      "tz_offset" => offset,
      "sent_at_local" => local_datetime
    }

    changeset =
      Pulse.changeset(%Pulse{}, params)
      |> Changeset.put_change(:user_id, user.id)
      |> Changeset.put_change(:machine_id, machine.id)

    repo.insert(changeset)
  end

  @spec create_xps!(Ecto.Repo.t(), User.t(), Pulse.t(), list) :: [XP.t()] | no_return
  defp create_xps!(repo, %User{} = user, %Pulse{} = pulse, xps) when is_list(xps) do
    xps
    |> Enum.map(fn
      %{"language" => language, "xp" => xp}
      when is_binary(language) and is_integer(xp) and xp >= 0 ->
        create_xp!(repo, user, pulse, language, xp)

      _ ->
        raise "Invalid XP format."
    end)
    |> Enum.filter(&(&1 != :ignored))
  end

  @spec create_xp!(Ecto.Repo.t(), User.t(), Pulse.t(), String.t(), integer) :: XP.t() | :ignored
  defp create_xp!(repo, %User{} = user, %Pulse{} = pulse, language_name, xp)
       when is_binary(language_name) and is_integer(xp) and xp >= 0 do
    {:ok, language} = Language.get_or_create(repo, language_name)

    user_aliased_language = Enum.find(user.aliases, &(&1.source_id == language.id))

    if user_aliased_language != nil and user_aliased_language.target_id == nil do
      :ignored
    else
      # Try to get final language from user aliases first, then from global aliases
      final_language =
        cond do
          user_aliased_language != nil and user_aliased_language.target_id == language.id ->
            language

          user_aliased_language != nil ->
            repo.get(Language, user_aliased_language.target_id)

          language.alias_of_id != nil ->
            repo.get(Language, language.alias_of_id)

          true ->
            language
        end

      true_xp = if xp > Utils.get_conf(:max_xp_per_pulse), do: 1, else: xp

      item =
        XP.changeset(%XP{}, %{amount: true_xp})
        |> Changeset.put_change(:pulse_id, pulse.id)
        |> Changeset.put_change(:language_id, final_language.id)
        |> Changeset.put_change(:original_language_id, language.id)
        |> repo.insert!()

      # Set final language into created XP so that it can be referenced later
      %{item | language: final_language}
    end
  end

  @spec get_offset(String.t()) :: {:ok, integer} | {:error, atom, String.t()}
  defp get_offset(timestamp) do
    # Get offset from an RFC3339 or ISO8601 string.
    timestamp = timestamp |> String.trim() |> String.downcase()

    if String.ends_with?(timestamp, "z") do
      {:ok, 0}
    else
      case Regex.run(@rfc3339_offset_regex, timestamp) do
        [_, sign, hours, minutes] -> {:ok, calculate_offset(sign, hours, minutes)}
        _ -> {:error, :generic, "Invalid TZ offset!"}
      end
    end
  end

  @spec calculate_offset(String.t(), String.t(), String.t()) :: integer
  defp calculate_offset(sign, hours, minutes)

  defp calculate_offset("+", hours, minutes), do: calculate_offset(hours, minutes)

  defp calculate_offset("-", hours, minutes), do: -calculate_offset(hours, minutes)

  @spec calculate_offset(String.t(), String.t()) :: integer
  defp calculate_offset(hours, minutes) do
    {hours, _} = Integer.parse(hours)
    {minutes, _} = Integer.parse(minutes)

    hours * 60 + minutes
  end
end
