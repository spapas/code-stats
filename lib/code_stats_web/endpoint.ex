defmodule CodeStatsWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :code_stats

  @session_options [
    store: :cookie,
    key: "_code_stats_key",
    signing_salt: "UuJXllxk"
  ]

  socket("/profile-live", Phoenix.LiveView.Socket,
    websocket: [connect_info: [session: @session_options]]
  )

  socket("/live_update_socket", CodeStatsWeb.LiveUpdateSocket, websocket: true)

  plug(CodeStatsWeb.RequestTimePlug)
  plug(CodeStatsWeb.FLoCPlug)

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phoenix.digest
  # when deploying your static files in production.
  plug(
    Plug.Static,
    at: "/",
    from: :code_stats,
    gzip: true,
    only: ~w(assets js css vendor favicon.ico robots.txt)
  )

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket("/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket)
    plug(Phoenix.LiveReloader)
    plug(Phoenix.CodeReloader)
  end

  plug(RemoteIp)

  plug(Plug.RequestId)
  plug(Plug.Logger)

  plug(
    Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Jason
  )

  plug(Plug.MethodOverride)

  plug(CodeStatsWeb.CORS)

  plug(Plug.Head)

  plug(
    Plug.Session,
    @session_options
  )

  plug(CodeStatsWeb.Router)
end
