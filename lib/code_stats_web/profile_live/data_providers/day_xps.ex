defmodule CodeStatsWeb.ProfileLive.DataProviders.DayXPs do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.User
  alias CodeStats.User.Pulse

  @behaviour CodeStatsWeb.ProfileLive.DataProviders.Behaviour

  defmodule Data do
    @type day_t :: 1..366

    deftypedstruct(%{
      days: %{optional(day_t()) => integer()}
    })
  end

  @impl true
  @spec required_data() :: MapSet.t(CodeStatsWeb.ProfileLive.DataProviders.SharedData.data_key())
  def required_data(), do: MapSet.new([:cache])

  @impl true
  @spec retrieve(CodeStatsWeb.ProfileLive.DataProviders.SharedData.t(), User.t()) :: Data.t()
  def retrieve(shared_data, _user) do
    days =
      Enum.reduce(shared_data.cache.dates, %{}, fn {date, xps}, acc ->
        adjusted_doy = get_doy(date)
        Map.update(acc, adjusted_doy, xps, &(&1 + xps))
      end)

    %Data{
      days: days
    }
  end

  @impl true
  @spec update(Data.t(), User.t(), Pulse.t(), User.Cache.t()) :: Data.t()
  def update(data, _user, pulse, _cache) do
    adjusted_doy = pulse.sent_at_local |> NaiveDateTime.to_date() |> get_doy()
    xps = Enum.reduce(pulse.xps, 0, fn x, sum -> sum + x.amount end)

    %Data{
      days: Map.update(data.days, adjusted_doy, xps, &(&1 + xps))
    }
  end

  @spec get_doy(Date.t()) :: Data.day_t()
  defp get_doy(date) do
    doy = Date.day_of_year(date)
    if not Date.leap_year?(date) and date.month >= 3, do: doy + 1, else: doy
  end
end
