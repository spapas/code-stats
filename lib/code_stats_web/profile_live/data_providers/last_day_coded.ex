defmodule CodeStatsWeb.ProfileLive.DataProviders.LastDayCoded do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.User
  alias CodeStats.User.Pulse

  @behaviour CodeStatsWeb.ProfileLive.DataProviders.Behaviour

  defmodule Data do
    deftypedstruct(%{
      last_day_coded: Date.t() | nil
    })
  end

  @impl true
  @spec required_data() :: MapSet.t(CodeStatsWeb.ProfileLive.DataProviders.SharedData.data_key())
  def required_data(), do: MapSet.new([:cache])

  @impl true
  @spec retrieve(CodeStatsWeb.ProfileLive.DataProviders.SharedData.t(), User.t()) :: Data.t()
  def retrieve(shared_data, _user) do
    max_date = shared_data.cache.dates |> Map.keys() |> Enum.max(Date, fn -> nil end)

    %Data{
      last_day_coded: max_date
    }
  end

  @impl true
  @spec update(Data.t(), User.t(), Pulse.t(), User.Cache.t()) :: Data.t()
  def update(data, _user, pulse, _cache) do
    new_date = NaiveDateTime.to_date(pulse.sent_at_local)

    max_date =
      if is_nil(data.last_day_coded) do
        new_date
      else
        Enum.max([data.last_day_coded, new_date], Date)
      end

    %Data{
      last_day_coded: max_date
    }
  end
end
