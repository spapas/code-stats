defmodule CodeStatsWeb.ProfileLive.DataProviders.SharedData do
  import CodeStats.Utils.TypedStruct

  alias CodeStats.User
  alias CodeStats.User.Pulse
  alias CodeStats.User.Cache
  alias CodeStats.Profile.Queries
  alias CodeStatsWeb.ProfileUtils

  @type data_key :: :recent_pulses | :cache

  deftypedstruct(%{
    recent_pulses: [Pulse.t()],
    cache: Cache.t()
  })

  @spec get_data_generators() :: %{optional(data_key()) => (User.t() -> any())}
  def get_data_generators() do
    %{
      recent_pulses: &get_recent_pulses/1,
      cache: &get_cache/1
    }
  end

  @spec get_data(MapSet.t(data_key()), User.t()) :: t()
  def get_data(required_data, user) do
    generators = get_data_generators()

    data =
      required_data
      |> MapSet.to_list()
      |> Enum.reduce(%{}, fn d, acc ->
        generator = Map.fetch!(generators, d)
        Map.put(acc, d, generator.(user))
      end)

    struct!(__MODULE__, data)
  end

  @spec get_recent_pulses(User.t()) :: [Pulse.t()]
  defp get_recent_pulses(user) do
    now = DateTime.utc_now()
    then = DateTime.add(now, -(3600 * ProfileUtils.recent_xp_hours()))
    Queries.pulses_since(user, then)
  end

  @spec get_cache(User.t()) :: Cache.t()
  defp get_cache(user) do
    CodeStats.User.CacheUtils.unformat_cache_from_db(user.cache)
  end
end
