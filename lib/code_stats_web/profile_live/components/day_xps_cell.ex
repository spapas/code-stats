defmodule CodeStatsWeb.ProfileLive.Components.DayXPsCell do
  use CodeStatsWeb, :live_component

  @moduledoc """
  A single cell of the "day XPs" table.

  Requires:
    @id
    @doy
    @xp
    @max_xp - Maximum XP of all cells.
  """

  @type color :: %{r: pos_integer(), g: pos_integer(), b: pos_integer()}

  @min_color %{r: 255, g: 255, b: 255}
  @max_color %{r: 62, g: 64, b: 83}
  @min_text_color %{r: 0, g: 0, b: 0}
  @max_text_color %{r: 252, g: 252, b: 254}

  @accent_text_flip_percent 50

  @impl true
  def render(assigns) do
    ~H"""
    <td style={cell_styles(@xp, @max_xp)} title={title(@doy, @xp)}>
      <%= cell_xp(@xp) %>
    </td>
    """
  end

  @spec cell_styles(integer(), integer()) :: String.t()
  defp cell_styles(xp, max_xp) do
    "background-color: rgb(#{bg_color(xp, max_xp)}); color: rgb(#{color(xp, max_xp)})"
  end

  @spec bg_color(integer(), integer()) :: String.t()
  defp bg_color(xp, max_xp)

  defp bg_color(_, 0) do
    "#{@min_color.r}, #{@min_color.g}, #{@min_color.b}"
  end

  defp bg_color(xp, max_xp) do
    scale = xp / max_xp
    scaled_color = scale_color(@min_color, @max_color, scale)
    "#{scaled_color.r}, #{scaled_color.g}, #{scaled_color.b}"
  end

  @spec color(integer(), integer()) :: String.t()
  defp color(xp, max_xp)

  defp color(_, 0) do
    "#{@min_text_color.r}, #{@min_text_color.g}, #{@min_text_color.b}"
  end

  defp color(xp, max_xp) when xp / max_xp * 100 >= @accent_text_flip_percent do
    "#{@max_text_color.r}, #{@max_text_color.g}, #{@max_text_color.b}"
  end

  defp color(_, _) do
    "#{@min_text_color.r}, #{@min_text_color.g}, #{@min_text_color.b}"
  end

  @spec title(CodeStatsWeb.ProfileLive.DataProviders.DayXPs.Data.day_t(), integer()) :: String.t()
  defp title(doy, xp) do
    date = CodeStats.DateUtils.date_from_day_of_leap_year(doy)
    date_str = Calendar.strftime(date, "%b %-d")
    "#{date_str}: #{CodeStatsWeb.ViewUtils.format_xp(xp)} XP"
  end

  @spec cell_xp(integer()) :: String.t()
  defp cell_xp(xp)
  defp cell_xp(xp) when abs(xp) < 1_000, do: xp
  defp cell_xp(xp) when abs(xp) < 1_000_000, do: "#{trunc(xp / 1_000)}k"
  defp cell_xp(xp), do: "#{trunc(xp / 1_000_000)}M"

  @spec scale_color(color(), color(), float()) :: color()
  defp scale_color(min_color, max_color, scale) do
    %{
      r: round(min_color.r + (max_color.r - min_color.r) * scale),
      g: round(min_color.g + (max_color.g - min_color.g) * scale),
      b: round(min_color.b + (max_color.b - min_color.b) * scale)
    }
  end
end
