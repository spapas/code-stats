defmodule CodeStatsWeb.ProfileLive.Components.LevelText do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ViewUtils

  @moduledoc """
  Level text that is part of headings. Need to prefix this with "Level" or
  "<language> level".

  Requires:
    @total_xp
    @new_xp
  """

  @impl true
  def render(assigns) do
    ~H"""
    <span class="level-text">
      <%= CodeStats.XP.XPCalculator.get_level(@total_xp) %> (<%= ViewUtils.format_xp(@total_xp) %>&nbsp;XP)
      <%= if @new_xp > 0 do %>
        (+<%= ViewUtils.format_xp(@new_xp) %>)
      <% end %>
    </span>
    """
  end
end
