defmodule CodeStatsWeb.ProfileLive.Components.ProgressBar do
  use CodeStatsWeb, :live_component

  @moduledoc """
  Simple progress bar with recent progress.

  Requires:
    @total_xp
    @new_xp
    @widths
  """

  @impl true
  def render(assigns) do
    ~H"""
    <%!--
      Due to how our progress bars work, we need to only apply the stacked class if
      there are two parts in the bar.
    --%>

    <div class={"progress-bar #{if elem(@widths, 0) > 0 and elem(@widths, 1) > 0, do: "stacked"}"}>
      <%= if elem(@widths, 0) > 0 do %>
        <span
          class="progress progress-old"
          role="progressbar"
          style={"width: #{elem(@widths, 0)}%"}
          aria-valuetext={"Level progress #{elem(@widths, 0)} %."}
        >
        </span>
      <% end %>

      <%= if elem(@widths, 1) > 0 do %>
        <span
          class="progress progress-recent"
          role="progressbar"
          style={"width: #{elem(@widths, 1)}%"}
          aria-valuetext={"Recent level progress #{elem(@widths, 1)} %."}
        >
        </span>
      <% end %>

      <span class="total-progress" aria-hidden="true">
        <%= elem(@widths, 0) + elem(@widths, 1) %>&nbsp;%
      </span>
    </div>
    """
  end
end
