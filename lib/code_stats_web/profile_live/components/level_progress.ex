defmodule CodeStatsWeb.ProfileLive.Components.LevelProgress do
  use CodeStatsWeb, :live_component

  alias CodeStats.XP.XPCalculator
  alias CodeStatsWeb.ProfileLive.Components

  @moduledoc """
  Level progress in a single target (language, machine…).

  Requires:
    @target
    @total_xp
    @new_xp
    @heading_type (element used for heading)
    @id (HTML id)
  """

  @impl true
  def render(assigns)

  def render(%{heading_type: "h4"} = assigns) do
    ~H"""
    <div class="level-progress">
      <h4 class="level-counter">
        <strong class="level-prefix"><%= @target %></strong>
        level <%= live_component(
          Components.LevelText,
          id: "#{@id}-level-text",
          total_xp: @total_xp,
          new_xp: @new_xp
        ) %>
      </h4>

      <%= live_component(
        Components.ProgressBar,
        id: "#{@id}-progress-bar",
        total_xp: @total_xp,
        new_xp: @new_xp,
        widths: get_xp_bar_widths(@total_xp, @new_xp)
      ) %>
    </div>
    """
  end

  def render(%{heading_type: "p"} = assigns) do
    ~H"""
    <div class="level-progress">
      <p class="level-counter">
        <strong class="level-prefix"><%= @target %></strong>
        level <%= live_component(
          Components.LevelText,
          id: "#{@id}-level-text",
          total_xp: @total_xp,
          new_xp: @new_xp
        ) %>
      </p>

      <%= live_component(
        Components.ProgressBar,
        id: "#{@id}-progress-bar",
        total_xp: @total_xp,
        new_xp: @new_xp,
        widths: get_xp_bar_widths(@total_xp, @new_xp)
      ) %>
    </div>
    """
  end

  @spec get_xp_bar_widths(integer(), integer()) :: {integer(), integer()}
  def get_xp_bar_widths(total_xp, new_xp) do
    level = XPCalculator.get_level(total_xp)
    current_level_xp = XPCalculator.get_next_level_xp(level - 1)

    have_xp = total_xp - current_level_xp

    if have_xp > new_xp do
      {
        XPCalculator.get_level_progress(total_xp - new_xp),
        XPCalculator.get_level_progress(total_xp) -
          XPCalculator.get_level_progress(total_xp - new_xp)
      }
    else
      {
        0,
        XPCalculator.get_level_progress(total_xp)
      }
    end
  end
end
