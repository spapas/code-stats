defmodule CodeStatsWeb.ProfileLive.Components.TotalProgress do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.Components

  @moduledoc """
  Total progress for an entity (e.g. user) across all languages.

  Requires:
    @total_xp
    @new_xp
    @heading_type (element used for heading)
  """

  @impl true
  def render(assigns) do
    ~H"""
    <div class="total-progress">
      <h2>
        Level <%= live_component(
          Components.LevelText,
          id: "total-progress-level-text",
          total_xp: @total_xp,
          new_xp: @new_xp
        ) %>
      </h2>

      <%= live_component(
        Components.ProgressBar,
        id: "total-progress-progress-bar",
        total_xp: @total_xp,
        new_xp: @new_xp,
        widths: Components.LevelProgress.get_xp_bar_widths(@total_xp, @new_xp)
      ) %>
    </div>
    """
  end
end
