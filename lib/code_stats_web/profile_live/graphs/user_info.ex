defmodule CodeStatsWeb.ProfileLive.Graphs.UserInfo do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.DataProviders
  alias CodeStatsWeb.ProfileLive.Graphs
  alias CodeStatsWeb.ProfileLive.Components
  alias Phoenix.LiveView

  @behaviour Graphs.Behaviour

  @impl true
  @spec providers() :: MapSet.t(module())
  def providers(),
    do: MapSet.new([DataProviders.UserInfo, DataProviders.TotalXP, DataProviders.LastDayCoded])

  @impl true
  @spec render_wrapper(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
  def render_wrapper(assigns) do
    ~H"""
    <.live_component
      module={__MODULE__}
      id={:user_info_graph}
      username={assigns[DataProviders.UserInfo].username}
      inserted_at={assigns[DataProviders.UserInfo].inserted_at}
      gravatar_email={assigns[DataProviders.UserInfo].gravatar_email}
      total_xp={assigns[DataProviders.TotalXP].total_xp}
      recent_xp={assigns[DataProviders.TotalXP].recent_xp}
      last_day_coded={assigns[DataProviders.LastDayCoded].last_day_coded}
    />
    """
  end
end
