defmodule CodeStatsWeb.ProfileLive.Graphs.Behaviour do
  alias Phoenix.LiveView

  @doc """
  Data providers required by this graph.
  """
  @callback providers() :: MapSet.t(module())

  @doc """
  Wrapper for the graph render call. Should pick off any assings related to this graph and render
  the graph with those assigns.
  """
  @callback render_wrapper(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
end
