defmodule CodeStatsWeb.ProfileLive.Graphs.Catalogue do
  import Phoenix.Component, only: [sigil_H: 2, assign: 3]

  alias CodeStatsWeb.ProfileLive.Graphs
  alias CodeStatsWeb.ProfileLive.DataProviders.SharedData
  alias CodeStatsWeb.ProfileLive.DataProviders.CommonTypes
  alias CodeStats.User.Flow
  alias Phoenix.LiveView

  @spec totals_graph() :: module()
  def totals_graph() do
    Graphs.UserInfo
  end

  @spec other_graphs() :: [module()]
  def other_graphs() do
    [
      Graphs.LastWeeks,
      Graphs.TopLanguages,
      Graphs.TopMachines,
      Graphs.OtherLanguages,
      Graphs.DayXPs,
      Graphs.TopFlows,
      Graphs.DayInfo,
      Graphs.TopFlowLanguages,
      Graphs.TopHours,
      Graphs.Ad
    ]
  end

  @spec data_providers([module()]) :: MapSet.t(module())
  def data_providers(graphs) do
    Enum.reduce(graphs, MapSet.new(), &MapSet.union(&2, &1.providers()))
    # Last day coded is required by main view
    |> MapSet.put(CodeStatsWeb.ProfileLive.DataProviders.LastDayCoded)
  end

  @spec get_initial_data(SharedData.t(), MapSet.t(module()), User.t()) ::
          CommonTypes.provider_data()
  def get_initial_data(shared_data, data_providers, user) do
    for provider <- data_providers, into: %{} do
      {provider, provider.retrieve(shared_data, user)}
    end
  end

  @spec assign_datas(
          LiveView.Socket.t(),
          MapSet.t(module()),
          CommonTypes.provider_data()
        ) ::
          LiveView.Socket.t()
  def assign_datas(socket, data_providers, data) do
    Enum.reduce(data_providers, socket, fn provider, s ->
      assign(s, provider, Map.fetch!(data, provider))
    end)
  end

  @spec get_assigned_datas(LiveView.Socket.t(), MapSet.t(module())) ::
          CommonTypes.provider_data()
  def get_assigned_datas(socket, data_providers) do
    Enum.reduce(data_providers, %{}, fn provider, acc ->
      val = Map.fetch!(socket.assigns, provider)
      Map.put(acc, provider, val)
    end)
  end

  @spec render_graphs(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
  def render_graphs(assigns) do
    ~H"""
    <div id="main-stats-container" class={["stripe"] ++ if @show_ads?, do: ["show-ad"], else: []}>
      <%= for graph <- @other_graphs do %>
        <%= graph.render_wrapper(assigns) %>
      <% end %>

      <div id="main-stats-explainer">
        <small>
          A flow state is defined as a streak of programming, lasting for at least <%= Flow.min_length() %> minutes, with pauses that last at most <%= Flow.max_pause() %> minutes, and gaining at least <%= Flow.min_xp_ratio() %> XP per minute.
        </small>
      </div>
    </div>
    """
  end
end
