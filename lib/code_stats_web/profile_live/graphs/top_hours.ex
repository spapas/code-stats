defmodule CodeStatsWeb.ProfileLive.Graphs.TopHours do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.DataProviders
  alias CodeStatsWeb.ProfileLive.Graphs
  alias Phoenix.LiveView

  @behaviour Graphs.Behaviour

  @impl true
  @spec providers() :: MapSet.t(module())
  def providers(),
    do: MapSet.new([DataProviders.TopXPHours, DataProviders.TopFlowHours])

  @impl true
  @spec render_wrapper(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
  def render_wrapper(assigns) do
    ~H"""
    <%= if assigns[DataProviders.TopXPHours].data != [] or assigns[DataProviders.TopFlowHours].data != [] do %>
      <.live_component
        module={__MODULE__}
        id={:top_hours_graph}
        xps={assigns[DataProviders.TopXPHours].data}
        flows={assigns[DataProviders.TopFlowHours].data}
      />
    <% else %>
      <div class="no-data"></div>
    <% end %>
    """
  end

  @impl true
  def render(assigns) do
    ~H"""
    <section id="top-hours" class="top-hours" phx-hook="TopHours">
      <h4>Most prolific hours of the day</h4>

      <div id="top-hours-dataset">
        ({x:<.xps_data xps={@xps} />,
        f:<.flows_data flows={@flows} />})
      </div>

      <div id="top-hours-chart" phx-update="ignore"></div>
    </section>
    """
  end

  defp xps_data(assigns) do
    ~H"""
    [
    <%= for {h, xp} <- @xps do %>
      {h:"<%= h %>",x:<%= xp %>},
    <% end %>
    ]
    """
  end

  defp flows_data(assigns) do
    ~H"""
    [
    <%= for {h, duration} <- @flows do %>
      {h:"<%= h %>",m:<%= duration %>},
    <% end %>
    ]
    """
  end
end
