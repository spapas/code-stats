defmodule CodeStatsWeb.ProfileLive.Graphs.TopFlowLanguages do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.DataProviders
  alias CodeStatsWeb.ProfileLive.Graphs
  alias Phoenix.LiveView
  alias CodeStatsWeb.ProfileLive.Components.CircleProgress

  @behaviour Graphs.Behaviour

  @impl true
  @spec providers() :: MapSet.t(module())
  def providers(),
    do: MapSet.new([DataProviders.TopFlowLanguages])

  @impl true
  @spec render_wrapper(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
  def render_wrapper(assigns) do
    ~H"""
    <%= if assigns[DataProviders.TopFlowLanguages].data != [] do %>
      <.live_component
        module={__MODULE__}
        id={:top_flow_languages_graph}
        data={Enum.slice(assigns[DataProviders.TopFlowLanguages].data, 0..9)}
      />
    <% else %>
      <div class="no-data"></div>
    <% end %>
    """
  end
end
