defmodule CodeStatsWeb.ProfileLive.Graphs.OtherLanguages do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.DataProviders
  alias CodeStatsWeb.ProfileLive.Graphs
  alias CodeStatsWeb.ProfileLive.Components
  alias Phoenix.LiveView

  @behaviour Graphs.Behaviour

  @impl true
  @spec providers() :: MapSet.t(module())
  def providers(),
    do: MapSet.new([DataProviders.TopLanguages])

  @impl true
  def mount(socket) do
    {:ok,
     assign(socket, show_all: false, languages_amount: display_amount(false), has_more: false)}
  end

  @impl true
  def update(assigns, socket) do
    new_assigns =
      Map.put(
        assigns,
        :has_more,
        length(assigns.languages) > display_amount(false)
      )

    {:ok, assign(socket, new_assigns)}
  end

  @impl true
  def handle_event("toggle-all", _, socket) do
    new_val = !socket.assigns.show_all

    {:noreply, assign(socket, show_all: new_val, languages_amount: display_amount(new_val))}
  end

  @impl true
  @spec render_wrapper(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
  def render_wrapper(assigns) do
    ~H"""
    <%= if assigns[DataProviders.TopLanguages].other_languages != [] do %>
      <.live_component
        module={__MODULE__}
        id={:other_languages_graph}
        languages={assigns[DataProviders.TopLanguages].other_languages}
      />
    <% else %>
      <div class="no-data"></div>
    <% end %>
    """
  end

  defp display_amount(true), do: 10_000_000
  defp display_amount(false), do: 10
end
