defmodule CodeStatsWeb.ProfileLive.Graphs.LastWeeks do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.DataProviders
  alias CodeStatsWeb.ProfileLive.Graphs
  alias Phoenix.LiveView
  alias CodeStatsWeb.ProfileLive.Graphs.Utils

  @behaviour Graphs.Behaviour

  @others_colour "#888"

  @impl true
  @spec providers() :: MapSet.t(module())
  def providers(),
    do: MapSet.new([DataProviders.LastWeeksXPs, DataProviders.LastWeeksFlows])

  @impl true
  @spec render_wrapper(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
  def render_wrapper(assigns) do
    ~H"""
    <%= if assigns[DataProviders.LastWeeksXPs].dataset != [] or assigns[DataProviders.LastWeeksFlows].data != %{} do %>
      <.live_component
        module={__MODULE__}
        id={:last_weeks_graph}
        xps={assigns[DataProviders.LastWeeksXPs].dataset}
        flows={assigns[DataProviders.LastWeeksFlows].data}
      />
    <% else %>
      <div class="no-data"></div>
    <% end %>
    """
  end

  @impl true
  def render(assigns) do
    ~H"""
    <section id="week-languages" class="week-languages" phx-hook="LastWeeks">
      <div id="week-languages-dataset">
        ({x:<.xp_data xps={@xps} />,
        f:<.flow_data flows={@flows} /> })
      </div>

      <div id="week-languages-chart" phx-update="ignore"></div>
    </section>
    """
  end

  defp xp_data(assigns) do
    ~H"""
    [
    <%= for {language, data} <- @xps do %>
      [{l:<%= Jason.encode!(language) %>,c:<%= ~s|"#{language_colour(language)}"| %>},[
      <%= for %{date: date, xp: xp} <- data do %>
        <.xp_row date={date} xp={xp} />,
      <% end %>
      ]],
    <% end %>
    ]
    """
  end

  defp xp_row(assigns) do
    ~H"""
    {x:<%= ~s|"#{Date.to_iso8601(@date)}"| %>,y:<%= @xp %>}
    """
  end

  defp flow_data(assigns) do
    ~H"""
    [
    <%= for {date, flows} <- @flows do %>
      {x:<%= ~s|"#{Date.to_iso8601(date)}"| %>,
      y:<%= flows |> Enum.map(& &1.duration) |> Enum.sum() %>},
    <% end %>
    ]
    """
  end

  @spec language_colour(String.t()) :: String.t()
  defp language_colour(language)

  defp language_colour("Others"), do: @others_colour

  defp language_colour(language) do
    Utils.language_colour(language)
  end
end
