defmodule CodeStatsWeb.ProfileLive.Graphs.TopLanguages do
  use CodeStatsWeb, :live_component

  alias CodeStatsWeb.ProfileLive.DataProviders
  alias CodeStatsWeb.ProfileLive.Graphs
  alias CodeStatsWeb.ProfileLive.Components
  alias Phoenix.LiveView

  @behaviour Graphs.Behaviour

  @impl true
  @spec providers() :: MapSet.t(module())
  def providers(),
    do: MapSet.new([DataProviders.TopLanguages])

  @impl true
  @spec render_wrapper(LiveView.Socket.assigns()) :: LiveView.Rendered.t()
  def render_wrapper(assigns) do
    ~H"""
    <.live_component
      module={__MODULE__}
      id={:top_languages_graph}
      languages={assigns[DataProviders.TopLanguages].top_languages}
    />
    """
  end
end
