defmodule CodeStatsWeb.Components.Gravatar do
  use Phoenix.Component
  alias CodeStatsWeb.Router.Helpers, as: Routes

  @moduledoc """
  Gravatar element that shows an <img> tag to request a gravatar from the backend.
  """

  attr(:socket, Phoenix.LiveView.Socket, required: true)
  attr(:username, :string, required: true)

  def img(assigns) do
    ~H"""
    <img
      src={Routes.profile_path(@socket, :profile_gravatar, @username)}
      alt={"#{@username}'s gravatar"}
      class="avatar"
    />
    """
  end
end
