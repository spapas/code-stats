defmodule CodeStatsWeb.Components.Gumroad do
  use Phoenix.Component

  def button(assigns) do
    ~H"""
    <a
      class="button gumroad-link"
      href={"https://app.gumroad.com/l/" <> @product_permalink}
      target="_blank"
    >
      Subscribe on <span class="gumroad-name">Gumroad</span>
    </a>
    """
  end
end
