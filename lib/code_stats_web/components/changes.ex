defmodule CodeStatsWeb.Components.Changes do
  use Phoenix.Component

  def render_change(assigns) do
    ~H"""
    <h3>
      <%= @version %> –
      <time datetime={Date.to_iso8601(@date)}>
        <%= Calendar.strftime(@date, "%b %_2d, %Y") %>
      </time>
      – <%= @title %>
    </h3>

    <ul>
      <%= for item <- @items do %>
        <li><%= item %></li>
      <% end %>
    </ul>
    """
  end
end
