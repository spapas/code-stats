defmodule CodeStatsWeb.ChangesView do
  use CodeStatsWeb, :view
  import Phoenix.Component, only: [sigil_H: 2]

  @doc "Helper to cleanup code for profile pictures"
  @spec profile_pic(Plug.Conn.t(), String.t(), String.t()) :: any()
  def profile_pic(conn, img, title) do
    render(
      CodeStatsWeb.ComponentsView,
      "picture.html",
      conn: conn,
      src: "/assets/frontend/images/contributors/#{img}",
      alt: title,
      title: title,
      img_class: "avatar"
    )
  end

  @spec render_change({String.t(), Date.t(), String.t(), [any()]}) :: any()
  def render_change({version, date, title, items}) do
    # Stupid sigil H requires assigns
    assigns = %{
      version: version,
      date: date,
      title: title,
      items: items
    }

    ~H"""
    <h3>
      <%= @version %> –
      <time datetime={Date.to_iso8601(@date)}>
        <%= Calendar.strftime(@date, "%b %_2d, %Y") %>
      </time>
      – <%= @title %>
    </h3>

    <ul>
      <%= for item <- @items do %>
        <li><%= item %></li>
      <% end %>
    </ul>
    """
  end
end
