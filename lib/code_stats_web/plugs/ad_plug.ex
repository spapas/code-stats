defmodule CodeStatsWeb.AdPlug do
  @moduledoc """
  Plug to control if ads are shown for the current user.
  """

  alias CodeStats.Utils
  alias CodeStats.PaidUser
  alias CodeStats.User.Paid.Level
  alias CodeStats.User.Paid.LevelUtils

  @behaviour Plug

  @impl Plug
  def init(opts), do: opts

  @impl Plug
  @spec call(Plug.Conn.t(), any()) :: Plug.Conn.t()
  def call(conn, _opts) do
    paid_user = CodeStatsWeb.AuthUtils.get_current_paid_user(conn)

    # This will be overridden on profile page where we know the viewed user
    Plug.Conn.assign(conn, :show_ads?, show_ad?(paid_user, nil))
  end

  @doc """
  Should an ad be shown, given the current user and the user being viewed?
  """
  @spec show_ad?(PaidUser.t() | nil, PaidUser.t() | nil) :: boolean()
  def show_ad?(current_user, viewing_user) do
    ads_enabled = Utils.get_conf(:ads_enabled)

    current_user_show_ads = LevelUtils.level_for_user(current_user) == Level.none()
    viewing_user_level = LevelUtils.level_for_user(viewing_user)

    ads_enabled and current_user_show_ads and viewing_user_level != Level.adfree_all()
  end
end
