defmodule CodeStats.Repo.Migrations.RemoveXpsTimestamps do
  use Ecto.Migration

  def change do
    alter table(:xps) do
      remove(:inserted_at, :utc_datetime, default: DateTime.utc_now())
      remove(:updated_at, :utc_datetime, default: DateTime.utc_now())
    end
  end
end
