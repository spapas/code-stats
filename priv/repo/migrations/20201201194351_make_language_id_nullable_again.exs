defmodule CodeStats.Repo.Migrations.MakeLanguageIdNullableAgain do
  use Ecto.Migration

  def change do
    alter table(:xps) do
      modify(:language_id, :integer, null: true)
    end
  end
end
