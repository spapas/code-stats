defmodule CodeStats.Repo.Migrations.AddPaid do
  use Ecto.Migration

  def change do
    create table(:products) do
      add(:product_id, :text, null: false)
      add(:variant_name, :text, null: false)
      add(:unlock_level, :text, null: false)
    end

    alter table(:users) do
      add(:paid_level, :text, null: false, default: "user_level_none")
      add(:license_key, :text, null: true, default: nil)
      add(:subscription_id, :text, null: true, default: nil)

      add(:paid_product_id, references(:products, on_delete: :restrict))
    end

    create(unique_index(:products, [:product_id, :variant_name]))
    create(index(:users, [:paid_product_id]))
    create(unique_index(:users, [:license_key]))
  end
end
