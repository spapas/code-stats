defmodule CodeStats.Repo.Migrations.AddUserAliases do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add(:aliases, :map, null: true)
    end
  end
end
