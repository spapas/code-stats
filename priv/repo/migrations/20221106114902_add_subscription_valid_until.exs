defmodule CodeStats.Repo.Migrations.AddSubscriptionValidUntil do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add(:subscription_valid_until, :date, null: true, default: nil)
    end

    create(index(:users, ["subscription_valid_until DESC"]))
  end
end
