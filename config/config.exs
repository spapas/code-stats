# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
import Config

# Configures the endpoint
config :code_stats, CodeStatsWeb.Endpoint,
  render_errors: [accepts: ~w(html json)],
  pubsub_server: CodeStats.PubSub,
  code_reloader: config_env() == :dev,
  debug_errors: config_env() == :dev,
  check_origin: config_env() == :prod

# Email config, override in your env.secret.exs
config :code_stats, CodeStats.Mailer,
  adapter: Bamboo.MailgunAdapter,
  hackney_opts: [ssl_options: [versions: [:"tlsv1.2"]]]

config :code_stats,
  ecto_repos: [CodeStats.Repo]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure phoenix generators
config :phoenix, :generators,
  migration: true,
  binary_id: false

config :comeonin, bcrypt_log_rounds: 12

config :number,
  delimiter: [
    precision: 0,
    delimiter: ",",
    separator: "."
  ]

config :geolix,
  init: {CodeStatsWeb.Geolix, :init}

# Appsignal configuration

config :phoenix, :template_engines,
  eex: Appsignal.Phoenix.Template.EExEngine,
  exs: Appsignal.Phoenix.Template.ExsEngine

# Store mix env used to compile app, since Mix won't be available in release
config :code_stats, compile_env: Mix.env()

# Store app version and commit hash at compile time
config :code_stats,
  commit_hash: System.cmd("git", ["rev-parse", "--verify", "--short", "HEAD"]) |> elem(0),
  version: Mix.Project.config()[:version]

config :mbu,
  auto_paths: true,
  create_out_paths: true,
  tmp_path: Path.expand(".tmp")

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Use TZData for time zone stuff in stdlib
config :elixir, :time_zone_database, Tzdata.TimeZoneDatabase

# Do not print debug messages in production
if config_env() == :prod do
  config :logger, level: :error
end
