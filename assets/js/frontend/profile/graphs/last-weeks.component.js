import { el } from "redom";
import { Chart } from "chart.js";
import { DateTime } from "luxon";

import { XP_FORMATTER } from "../../../common/xp_utils.js";
import { format_mins } from "../../../common/utils.js";
import { SMALL_BREAKPOINT } from "../../../common/config.js";

class LastWeeksComponent {
  constructor() {
    this.hasFlowDataset = false;

    this.el = el("canvas");
  }

  onmount() {
    const ads_shown = document
      .getElementById("main-stats-container")
      ?.classList.contains("show-ad");

    // Create chart only in onmount due to using multiple yaxes and this bug:
    // https://github.com/chartjs/Chart.js/issues/7761
    this.chart = new Chart(this.el.getContext("2d"), {
      type: "bar",
      data: {
        labels: [],
        datasets: [],
      },
      options: {
        plugins: {
          legend: {
            display: true,
            position: ads_shown ? "left" : "right",
          },
          tooltip: {
            enabled: true,
            displayColors: true,
            mode: "x",
            position: "nearest",
            // Filter out 0-languages from tooltip
            filter: (i) => Number.isFinite(i.raw.y) && i.raw.y > 0,
            callbacks: {
              title: () => "",
              // Format tooltip labels as XP
              label: (tooltip_item) => {
                if (tooltip_item.dataset.yAxisID === "y1") {
                  return `${format_mins(tooltip_item.parsed.y)} spent in flow`;
                } else {
                  return `${tooltip_item.dataset.label}: ${tooltip_item.formattedValue} XP`;
                }
              },
            },
          },
        },
        scales: {
          x: {
            stacked: true,
            type: "time",
            time: {
              unit: "day",
            },
          },
          y: {
            stacked: true,
            title: {
              display: true,
              text: "XP",
            },
            ticks: {
              callback: (val) => XP_FORMATTER.format(val),
              min: 0,
            },
            position: "left",
          },
        },
        maintainAspectRatio: false,
        onResize: (chart, { width }) => {
          if (width < SMALL_BREAKPOINT) {
            chart.options.plugins.legend.display = false;
          } else {
            chart.options.plugins.legend.display = true;
          }
        },
      },
    });

    this.labels = this.chart.data.labels;
    this.datasets = this.chart.data.datasets;
  }

  update({ x: day_language_xps, f: flow_mins_by_day }) {
    this.labels.length = 0;
    this.datasets.length = 0;

    const now = DateTime.local();
    const since_14d = now.minus({ weeks: 2, days: -1 });
    this.chart.options.scales.x.min = since_14d.toISODate();
    this.chart.options.scales.x.max = now.toISODate();

    // Loop through data and insert into datasets in the correct places
    for (const [{ l: language, c: colour }, datas] of day_language_xps) {
      const dataset = this._createDataset(language, colour);
      dataset.data = datas;
    }

    if (flow_mins_by_day.length > 0) {
      const dataset = this._createFlowDataset();
      dataset.data = flow_mins_by_day;
    }

    this.chart.update("none");
  }

  // Create a new dataset for given language
  _createDataset(lang, colour) {
    let label = lang;

    const length = this.datasets.push({
      data: [],
      backgroundColor: colour,
      label,
      type: "bar",
      yAxisID: "y",
    });

    return this.datasets[length - 1];
  }

  _createFlowDataset() {
    this.hasFlowDataset = true;

    this.chart.options.scales.y1 = {
      title: {
        display: true,
        text: "Flow time",
      },
      ticks: {
        callback: format_mins,
        min: 0,
      },
      position: "right",
      gridLines: {
        display: false,
      },
    };

    const flowDataset = {
      type: "line",
      data: [],
      label: "Flow time",
      yAxisID: "y1",
      showLine: false,
      pointRadius: 5,
      pointBackgroundColor: "rgba(0, 0, 0, 0)",
      pointBorderColor: "#ccc",
      pointBorderWidth: 2,
      order: -1,
    };

    this.datasets.push(flowDataset);
    return flowDataset;
  }
}

export { LastWeeksComponent };
