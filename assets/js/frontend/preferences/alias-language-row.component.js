import { el } from "redom";

import { XP_FORMATTER } from "../../common/xp_utils.js";

class AliasLanguageRowComponent {
  static getTitle(name) {
    return `Click to copy '${name}'`;
  }

  constructor(_, { name, xp }) {
    this.name = name;

    this.nameCell = el("td.language-name", this.name, {
      role: "button",
      title: AliasLanguageRowComponent.getTitle(this.name),
    });
    this.xpCell = el("td", XP_FORMATTER.format(xp));

    this.el = el("tr", [this.nameCell, this.xpCell]);

    this.nameCell.addEventListener("click", () => this.copy());
  }

  update({ name, xp }) {
    this.name = name;
    this.nameCell.textContent = name;
    this.nameCell.title = AliasLanguageRowComponent.getTitle(this.name);
    this.xpCell.textContent = XP_FORMATTER.format(xp);
  }

  async copy() {
    this.nameCell.textContent = "Copying...";
    await navigator.clipboard.writeText(this.name);
    this.nameCell.textContent = "Copied!";
    await new Promise((resolve) => setTimeout(resolve, 1000));
    this.nameCell.textContent = this.name;
  }
}

export { AliasLanguageRowComponent };
