import { el, list, mount, setChildren } from "redom";

import { request_profile } from "../../common/utils.js";
import { AliasRowComponent } from "./alias-row.component.js";
import { AliasLanguageRowComponent } from "./alias-language-row.component.js";
import LoadingIndicatorComponent from "../../common/loading-indicator.component.js";
import { graphButton } from "../../common/graph-buttons.js";

/** @typedef {{ id: string, source: string, target: string }} LanguageAlias */

function xpSorter({ xp: aXP }, { xp: bXP }) {
  return bXP - aXP;
}

class AliasEditorComponent {
  /**
   * @param {HTMLTextAreaElement} alias_textarea
   */
  constructor(alias_textarea) {
    this.textareaEl = alias_textarea;
    this.textareaEl.style.display = "none";

    this.username = document.getElementsByName("authed-username")[0].content;

    /** @type {LanguageAlias[]} */
    this.data = [];

    this.addButtonEl = el(
      "button#aliases-add.icon-button",
      { type: "button" },
      [graphButton("plus", "M3 0v3h-3v2h3v3h2v-3h3v-2h-3v-3h-2z", {})]
    );

    this.submitEl = document.getElementById("aliases-submit");
    this.formEl = document.getElementById("aliases-editor-form");
    this.listEl = list(el("div#aliases-list"), AliasRowComponent, null, {
      parent: this,
    });

    const languageListElCreator = () =>
      list(el("tbody"), AliasLanguageRowComponent, "name", null);

    this.beforeLanguagesEl = languageListElCreator();
    this.afterLanguagesEl = languageListElCreator();

    this.currentDataEl = el(
      "div#aliases-languages",
      new LoadingIndicatorComponent()
    );
    this.languageTables = [
      el("table", [
        el("thead", [
          el("tr", [el("th", "Before aliasing", { colspan: 2 })]),
          el("tr", [el("th", "Name"), el("th", "XP")]),
        ]),
        el("tfoot", [el("tr", [el("th", "Name"), el("th", "XP")])]),
        this.beforeLanguagesEl,
      ]),
      el("table", [
        el("thead", [
          el("tr", [el("th", "After aliasing", { colspan: 2 })]),
          el("tr", [el("th", "Name"), el("th", "XP")]),
        ]),
        el("tfoot", [el("tr", [el("th", "Name"), el("th", "XP")])]),
        this.afterLanguagesEl,
      ]),
    ];

    this.el = el("div", [
      this.listEl,
      this.addButtonEl,
      this.submitEl,
      el("h3", "Current data"),
      el(
        "p",
        el(
          "small",
          "Tip: Click on a language to copy its name to the clipboard."
        )
      ),
      this.currentDataEl,
    ]);

    this.addButtonEl.addEventListener("click", () => this.add());
    this.formEl.addEventListener("submit", () => this.submit());

    this.init();
  }

  init() {
    const content = JSON.parse(this.textareaEl.value);

    for (const { id, source, target } of content) {
      this.data.push({
        id,
        source,
        target,
      });
    }

    this.listEl.update(this.data);

    request_profile(this.username, {
      before: "unaliasedLanguages { name xp }",
      after: "languages(noCache:true) { name xp }",
    }).then((data) => this.updateLanguages(data));
  }

  add() {
    this.data.push({});
    this.updateInternal();
    this.listEl.update(this.data);
  }

  deleteChild(data) {
    const index = this.data.findIndex((item) => item === data);
    if (index !== -1) {
      this.data.splice(index, 1);
      this.updateInternal();
      this.listEl.update(this.data);
    }
  }

  updateInternal() {
    this.textareaEl.value = JSON.stringify(this.data);
  }

  updateLanguages({ before, after }) {
    before.sort(xpSorter);
    after.sort(xpSorter);

    this.beforeLanguagesEl.update(before);
    this.afterLanguagesEl.update(after);
    this.loadingLanguages = false;
    setChildren(this.currentDataEl, this.languageTables);
  }

  submit() {
    mount(
      this.el,
      new LoadingIndicatorComponent("Submitting…"),
      this.submitEl,
      true
    );
  }
}

export { AliasEditorComponent };
