import { wait_for_load } from "../common/utils.js";
import { mount } from "redom";
import OldTermsComponent from "./terms/old-terms.component.js";
import { initExport } from "./data_export_utils.js";

/**
 * Code to execute on legal terms page. Shows links to old terms and diffs to current versions.
 */
async function terms_page() {
  await wait_for_load();
  initExport();

  const terms_stripe_el = document.getElementById("terms-tabs");
  mount(terms_stripe_el, new OldTermsComponent());
}

export default terms_page;
