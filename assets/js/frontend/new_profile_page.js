import { LiveSocket } from "phoenix_live_view";
import { Socket } from "phoenix";
import topbar from "../vendor/topbar.js";
import { mount } from "redom";

import { LastWeeksComponent } from "./profile/graphs/last-weeks.component.js";
import { TopHoursComponent } from "./profile/graphs/top-hours.component.js";
import { importEthicalAds } from "../common/ethical-ads.js";

function chartHooks(chartElId, datasetElId, chartComponent) {
  let chart;
  let mounted = false;

  return {
    mounted() {
      const datasetEl = document.getElementById(datasetElId);
      const chartEl = document.getElementById(chartElId);

      if (datasetEl === null || chartEl === null) {
        return;
      }

      const dataset = eval(datasetEl.textContent);

      chart = new chartComponent();
      mount(chartEl, chart);
      mounted = true;
      chart.update(dataset);
    },

    updated() {
      const datasetEl = document.getElementById(datasetElId);
      const chartEl = document.getElementById(chartElId);

      if (datasetEl === null || chartEl === null) {
        return;
      }

      if (!mounted) {
        chart = new chartComponent();
        mount(chartEl, chart);
        mounted = true;
      }

      const dataset = eval(datasetEl.textContent);
      chart.update(dataset);
    },
  };
}

const hooks = {
  LastWeeks: chartHooks(
    "week-languages-chart",
    "week-languages-dataset",
    LastWeeksComponent
  ),
  TopHours: chartHooks(
    "top-hours-chart",
    "top-hours-dataset",
    TopHoursComponent
  ),
  AdHook: {
    async mounted() {
      try {
        await importEthicalAds();
        window.ethicalads.load();
      } catch (e) {
        console.error("Error loading ads:", e);
      }
    },
  },
};

export function new_profile_page() {
  const csrfToken = document
    .querySelector("meta[name='csrf-token']")
    .getAttribute("content");
  const liveSocket = new LiveSocket("/profile-live", Socket, {
    params: { _csrf_token: csrfToken },
    hooks,
  });

  topbar.config({
    barColors: { 0: "#29d" },
    shadowColor: "rgba(0, 0, 0, .3)",
  });
  window.addEventListener("phx:page-loading-start", (info) => topbar.show());
  window.addEventListener("phx:page-loading-stop", (info) => topbar.hide());

  liveSocket.connect();
}
