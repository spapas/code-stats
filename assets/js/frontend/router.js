import machine_page from "./machine_page.js";
import index_page from "./index_page.js";
import preferences_page from "./preferences_page.js";
import terms_page from "./terms_page.js";
import { new_profile_page } from "./new_profile_page.js";

/**
 * List of routes.
 *
 * Key should be regex to match against path, value should be function to execute.
 *
 * First matching route is executed.
 */
const ROUTES = [
  [/^\/users\/[^/]+\/?$/, new_profile_page],
  [/^\/my\/machines\/?$/, machine_page],
  [/^\/my\/preferences\/?/, preferences_page],
  [/^\/my\/consent\/?$/, terms_page],
  [/^\/?$/, index_page],
];

/**
 * The router executes the correct code based on the current path.
 */
class Router {
  constructor() {
    this.path = window.location.pathname;
  }

  execute() {
    for (const route of ROUTES) {
      if (route[0].test(this.path)) {
        route[1]();
        return;
      }
    }
  }
}

export default Router;
