let imported = false;

export async function importEthicalAds() {
  if (imported) {
    return;
  }

  imported = true;
  const url = "https://media.ethicalads.io/media/client/ethicalads.min.js";

  return new Promise((resolve, reject) => {
    const script = document.createElement("script");
    script.src = url;
    script.onload = resolve;
    script.onerror = reject;
    document.head.appendChild(script);
  });
}
